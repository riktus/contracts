//
//  Contracts.swift
//  SpendingCounter
//
//  Created by Александр  Волков on 07.06.2018.
//  Copyright © 2018 riktus. All rights reserved.
//

import Foundation

public class Contract {
    
    public static func isNotNil(obj: Any?) throws {
        if obj == nil {
            throw ContractErrors.argumentNilException
        }
    }
    
    public static func isNotEmpty(str: String) throws {
        if str == "" {
            throw ContractErrors.stringIsEmptyException
        }
    }
    
    public static func isNotEmpty(collection: [Any]) throws {
        if collection.count == 0 {
            throw ContractErrors.collectionIsEmptyException
        }
    }
    
    public static func isMoreThan(left: Int, right: Int) throws {
        if right > left {
            throw ContractErrors.argumentLessThanExpectedException
        }
    }
    
    public static func isRequired(handler: @escaping (() -> (Bool))) throws {
        if handler() == false {
            throw ContractErrors.requirementNotSatisfiedException
        }
    }
}
