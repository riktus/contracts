//
//  ContractErrors.swift
//  SpendingCounter
//
//  Created by Александр  Волков on 07.06.2018.
//  Copyright © 2018 riktus. All rights reserved.
//

import Foundation

enum ContractErrors : Error {
    case argumentNilException
    case argumentLessThanExpectedException
    case stringIsEmptyException
    case collectionIsEmptyException
    case requirementNotSatisfiedException
}
