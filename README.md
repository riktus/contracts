# Contracts

[![CI Status](https://img.shields.io/travis/riktus/Contracts.svg?style=flat)](https://travis-ci.org/riktus/Contracts)
[![Version](https://img.shields.io/cocoapods/v/Contracts.svg?style=flat)](https://cocoapods.org/pods/Contracts)
[![License](https://img.shields.io/cocoapods/l/Contracts.svg?style=flat)](https://cocoapods.org/pods/Contracts)
[![Platform](https://img.shields.io/cocoapods/p/Contracts.svg?style=flat)](https://cocoapods.org/pods/Contracts)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Contracts is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Contracts'
```

## Author

riktus, riktus@protonmail.com

## License

Contracts is available under the MIT license. See the LICENSE file for more info.
